# Introduction to Git

![git logo](https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/assets/logo@2x.png)

**The Introduction to Git MDbook is available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/index.html)**

**The slides are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/introduction-to-git/slides/#/)**

This module is an introduction on how to use git and GitLab.

**Git** is a distributed version control system that allows one or multiple people to coordinate work on a project.  

It allow users to track ***who*** changed ***what*** part of the project, ***when*** they changed it, and ***why***.  
 
**GitLab** is an online platform that provides a centralized location for git repositories.

**A git reference page is available [here](https://git-scm.com/docs)**
