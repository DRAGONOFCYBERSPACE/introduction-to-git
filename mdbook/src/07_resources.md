# Resources.md

**Read Me File:**  This file provides an introduction to the project and additional information that may be useful to collaborators, such as how to install the software, how to run any automated tests, how to use the code, and how to make contributions to the project.

![](assets/13.PNG)

---
### Additional Resources
* **[Learn more about GitLab]**(https://docs.gitlab.com/ee/README.html)
* **[Learn Git Branching]**(https://learngitbranching.js.org/)
* **[Atlassian: Become a Git Guru]**(https://www.atlassian.com/git/tutorials)
* **[Try Git]**(https://try.github.io/levels/1/challenges/1)
* **[Git: The Simple Guide]**(http://rogerdudler.github.io/git-guide/)
* **[GitDocs: Git Tutorial]**(https://git-scm.com/docs/gittutorial)
* **[Learn Git]**(https://www.tutorialspoint.com/git/index.htm)


