# Introduction to Gitlab CI/CD

## CI/CD

Gitlab has a very robust feature built in called CI/CD that helps with continuous methodologies for development. This tool helps with automation and helps developers focus more on development, instead of having to do the same things like testing and deploying over and over.

CI/CD stands for Continuous Integration(CI) / Continuous Delivery(CD) or Continuous Deployment(CD).

Essentially how this works is with Continuous Integration you push your code to a repository and every push to master (or any branch depending on setup) will run a pipeline of scripts to build, test, and validate code changes before merging them in. With Continuous Deployment if your scripts for CI passed then the Gitlab pipelines will automatically redeploy your application with the changes merged in.

These methodologies ensure that your project is bug free and all the code you push follows the coding standards that you or your team have setup. 

The CI/CD portion of Gitlab can be accessed in a Gitlab repository by selecting the "CI/CD" option on the left sidebar.

### ![](assets/cicd.png)

Would you like to know more? [CI/CD](https://docs.gitlab.com/ee/ci/introduction/index.html)

## YAML

YAML is a recursive acronym that stands for "Yaml Ain't Markup Language". YAML files are the backbone of Gitlab CI/CD. YAML is a human-readable data serialization standard that can be used in conjuction with all programming languages and is mainly used to write configuration files. So we use YAML files to configure our CI/CD process. 

In order to get a Pipeline started we have to create a YAML file in the root of the repository. This is done by adding a new file to the root of the repository and selecting a template for a .gitlab-ci.yml file.

NOTE: Gitlab gives you the option to select a template for your YAML file to get you started.

### ![](assets/newyaml.png)

Remember, the YAML file will hold the contents of how the pipeline should be run and its configuration. This includes Stages, Jobs, and how we create Artifacts. We'll get into all of those after we discuss Pipelines. 

Would you like to know more? [YAML](https://docs.gitlab.com/ee/ci/yaml/README.html)
___

You'll notice in the CI/CD image above we have a few options under the CI/CD option. Pipelines, Jobs, and Schedules. For now we'll discuss Pipelines and Jobs.


## Pipelines

So, what is a pipeline? Pipelines are the structure of your CI/CD process. Pipelines consist of one or more stages that run in order and can each contain one or more jobs that run in parallel. The jobs (or scripts) are executed by the Gitlab Runner agent. 

Below is an example of what a pipeline looks like in Gitlab.
### ![](assets/pipeline.png)

Its common for a pipeline to have four stages executed in the following order:
- A build stage
- A test stage
- A staging stage
- A production stage

Would you like to know more? [Pipelines](https://docs.gitlab.com/ee/ci/pipelines/index.html)
___

## Stages

Stages execute in whatever order you determine and are used to define the jobs that will be executed. The ordering of the jobs within the stage determines how they will be run.

In our YAML file we can determine how to order stages like this

```
stages:
    - build
    - test
    - deploy
```

All jobs within the build stage execute in parallel and if they pass then then the next stage starts and so on. Finally, if the deploy stage succeeds then the commit is marked as "passed".
If any of the jobs fail then the commit is marked as "failed" and no more jobs are executed.

Would you like to know more? [Stages](https://docs.gitlab.com/ee/ci/yaml/README.html#stages)

___

## Jobs

Jobs are the most fundamental element of the .gitlab-ci.yml file. We've discussed jobs quite a bit already and see how important they are within the pipeline. 

Jobs are:

- Defined with constraints stating under what conditions they should be executed.
- Top-level elements with an arbitrary name and must contain at least the script clause.
- Not limited in how many can be defined.

Here is how to set up a Job
```
job1:
  script: "execute-script-for-job1"

job2:
  script: "execute-script-for-job2"
```
This is the most basic setup for a job. Jobs can have many parameters associated with them including, services, rules, tags, caching, setting up docker images, exceptions and many more. 

NOTE: Be aware that there are some reserved keywords that can't be used as job names.

### ![](assets/unavailablejobnames.png)

Jobs have a very useful parameter when they finish where they can create artifacts. We'll discuss artifacts in the next section.

Would you like to know more? [Jobs](https://docs.gitlab.com/ee/ci/yaml/#configuration-parameters)

___

## Artifacts

Job Artifacts are files and directories that are created by a job once it is finished. This feature is enabled by default in any Gitlab installation.

Here is how to set up your artifact definition in the YAML file.

```
pdf:
  script: build_pdf_script.py
  artifacts:
    paths:
      - mypdf.pdf
    expire_in: 1 week
```

A job named pdf calls the build_pdf_scipt.py in order to build a PDF file. We then define the artifacts paths which in turn are defined with the paths keyword. All paths to files and directories are relative to the repository that was cloned during the build.

After your job has completed you can see your artifacts that were generated if you navigate to the job, and look at the right sidebar.

### ![](assets/artifactlocation.png)

Would you like to know more? [Artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)

___

## Completed YAML File

Now that we've seen all of the pieces of how to configure our YAML file, lets put it all together with a basic completed sample.

### ![](assets/basicpipeline.png)

The completed basic YAML file below represents the basic pipeline image above.
```
stages:
  - build
  - test
  - deploy

image: alpine

build_a:
  stage: build
  script:
    - echo "This job builds something."

build_b:
  stage: build
  script:
    - echo "This job builds something else."

test_a:
  stage: test
  script:
    - echo "This job tests something. It will only run when all jobs in the"
    - echo "build stage are complete."

test_b:
  stage: test
  script:
    - echo "This job tests something else. It will only run when all jobs in the"
    - echo "build stage are complete too. It will start at about the same time as test_a."

deploy_a:
  stage: deploy
  script:
    - echo "This job deploys something. It will only run when all jobs in the"
    - echo "test stage complete."

deploy_b:
  stage: deploy
  script:
    - echo "This job deploys something else. It will only run when all jobs in the"
    - echo "test stage complete. It will start at about the same time as deploy_a."
```

___

We have only scratched the surface here with Gitlab CI/CD. There are so many other useful things you can do with these .gitlab-ci.yml files, you only have to explore the documentation to find out.